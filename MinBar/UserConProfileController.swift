//
//  UserConProfileController.swift
//  PentorApp
//
//  Created by Segun Solaja on 3/28/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import APESuperHUD
import FirebaseDatabase
import FirebaseStorage

class UserConProfileController: BaseViewController {
    
    @IBOutlet weak var userProfileImage: UIImageView!
    
    @IBOutlet weak var userFullName: UILabel!
    
    @IBOutlet weak var userLongBio: UITextView!
    
    @IBOutlet weak var pendingStatus: UILabel!
    
    @IBOutlet weak var declineButton: UIButton!
    
    @IBOutlet weak var acceptButton: UIButton!
    
    var user:User?
    var parentController:UIViewController?
    var me: User? = User.archivedUser()
    var connectionId = ""
    var isMentee =  false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    func configureView() {
        guard let user = user else {
            return
        }
        self.userFullName.text = user.fullname()
        let longbio = user.longBio ?? ""
        self.userLongBio.text = longbio == .none ? "title.nodesc".localized() : longbio
        if let photourl  = user.profilePhotoURL {
            self.userProfileImage.sd_setImage(with: URL(string: photourl), placeholderImage: UIImage(named:"man"))
        }
        self.userProfileImage.clipsToBounds = true
        self.userProfileImage.contentMode = .scaleAspectFill
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.width / 2

        if(isMentee == true) {
            pendingStatus.isHidden = false
            declineButton.isHidden = true
            acceptButton.isHidden = true
        }else{
            pendingStatus.isHidden = true
            declineButton.isHidden = false
            acceptButton.isHidden = false
        }
        
        acceptButton.addTarget(self, action: #selector(acceptConnection), for: .touchUpInside)
        declineButton.addTarget(self, action: #selector(declineConnection), for: .touchUpInside)
        declineButton.layer.cornerRadius = 4
        acceptButton.layer.cornerRadius = 4
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title  =  isMentee == true ? "title.mentor".localized() : "title.mentee".localized()
    }
    
    func acceptConnection(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.sending".localized(), presentingView: self.view)
        
        let urlParamString = "/\(connectionId)?status-id=mn"
        UserService.acceptOrDeclineConnection(urlParam: urlParamString, resultCallback: {[weak self] (connection) in
            print(connection.currentStatus ?? "error")
            if let view = self?.view {
                APESuperHUD.removeHUD(animated: true, presentingView: view, completion: { _ in
                    // Completed
                    NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
                    self?.setupFirebaseChat()
                    let viewController = ChatViewController(collectionViewLayout: UICollectionViewFlowLayout())
                    viewController.title = "Chat"
                    viewController.userFullName = self?.userFullName.text ?? ""
                    viewController.connectionId = self?.connectionId ?? ""
                    viewController.email = self?.user?.email ?? ""
                    viewController.view.backgroundColor = .white

                    CATransaction.begin()
                    CATransaction.setCompletionBlock {
                        self?.parentController?.navigationItem.title = ""
                        self?.parentController?.navigationController?.pushViewController(viewController, animated: true)
                    }
                    _ = self?.navigationController?.popViewController(animated: true)

                    CATransaction.commit()
                })
            }
        }) { (err) in
            print(err)
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
        
        
        
    }
    
    
    
    func declineConnection(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.sending".localized(), presentingView: self.view)
        let urlParamString = "/\(connectionId)?status-id=dl"
        UserService.acceptOrDeclineConnection(urlParam: urlParamString, resultCallback: { (connection) in
            print(connection.currentStatus ?? "error")
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                // Completed
                NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
                _ = self.navigationController?.popViewController(animated: true)
                
            })
            
        }) { (err) in
            print(err)
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    
    func setupFirebaseChat(){
        guard let user = self.user else {
            return
        }
        guard let me = self.me else {
            return
        }
        let useremail = user.email ?? ""
        let meemail = me.email ?? ""
        let value = ["id": connectionId ]
        FirebaseConfig.connectionsRef.child(connectionId).setValue(value) { (err, ref) in
            let theUser = useremail.replacingOccurrences(of: ".", with: "%")
            let email = meemail
            let _me = email.replacingOccurrences(of: ".", with: "%")
            let val = ["\(theUser)":false,"\(_me)":false]
            ref.child("participants").setValue(val)
        }
        
    }
    
}
