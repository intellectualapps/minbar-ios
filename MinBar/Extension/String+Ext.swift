//
//  String+Ext.swift
//  MinBar
//
//  Created by Nguyen Van Dung on 9/14/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import Foundation
import UIKit

extension String {
    /*
     Get string localized in Locazable file.
     */
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
