//
//  UIDevice+Extension.swift
//  taxi_crew
//
//  Created by Nguyen Van Dung on 10/11/16.
//  Copyright © 2016 japantaxi.co.jp. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

public struct DVScreenSize {
    public static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    public static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    public static let SCREEN_MAX_LENGTH    = max(DVScreenSize.SCREEN_WIDTH, DVScreenSize.SCREEN_HEIGHT)
    public static let SCREEN_MIN_LENGTH    = min(DVScreenSize.SCREEN_WIDTH, DVScreenSize.SCREEN_HEIGHT)
}

public struct DeviceType {
    public static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && DVScreenSize.SCREEN_MAX_LENGTH < 568.0
    public static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && DVScreenSize.SCREEN_MAX_LENGTH == 568.0
    public static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && DVScreenSize.SCREEN_MAX_LENGTH == 667.0
    public static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && DVScreenSize.SCREEN_MAX_LENGTH == 736.0
    public static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && DVScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

public enum Hardware {
    case NOT_AVAILABLE
    
    case IPHONE_2G
    case IPHONE_3G
    case IPHONE_3GS
    
    case IPHONE_4
    case IPHONE_4_CDMA
    case IPHONE_4S
    
    case IPHONE_5
    case IPHONE_5_CDMA_GSM
    case IPHONE_5C
    case IPHONE_5C_CDMA_GSM
    case IPHONE_5S
    case IPHONE_5S_CDMA_GSM
    
    case IPHONE_6
    case IPHONE_6_PLUS
    case IPHONE_6S
    case IPHONE_6S_PLUS
    case IPHONE_SE
    
    case IPOD_TOUCH_1G
    case IPOD_TOUCH_2G
    case IPOD_TOUCH_3G
    case IPOD_TOUCH_4G
    case IPOD_TOUCH_5G
    case IPOD_TOUCH_6G
    
    case IPAD
    case IPAD_2
    case IPAD_2_WIFI
    case IPAD_2_CDMA
    case IPAD_3
    case IPAD_3G
    case IPAD_3_WIFI
    case IPAD_3_WIFI_CDMA
    case IPAD_4
    case IPAD_4_WIFI
    case IPAD_4_GSM_CDMA
    
    case IPAD_MINI
    case IPAD_MINI_WIFI
    case IPAD_MINI_WIFI_CDMA
    case IPAD_MINI_RETINA_WIFI
    case IPAD_MINI_RETINA_WIFI_CDMA
    case IPAD_MINI_3_WIFI
    case IPAD_MINI_3_WIFI_CELLULAR
    case IPAD_MINI_3_WIFI_CELLULAR_CN
    case IPAD_MINI_4_WIFI
    case IPAD_MINI_4_WIFI_CELLULAR
    case IPAD_MINI_RETINA_WIFI_CELLULAR_CN
    
    case IPAD_AIR_WIFI
    case IPAD_AIR_WIFI_GSM
    case IPAD_AIR_WIFI_CDMA
    case IPAD_AIR_2_WIFI
    case IPAD_AIR_2_WIFI_CELLULAR
    
    case IPAD_PRO_97_WIFI
    case IPAD_PRO_97_WIFI_CELLULAR
    case IPAD_PRO_WIFI
    case IPAD_PRO_WIFI_CELLULAR
    
    case SIMULATOR
}

public extension UIDevice {
    public class func isSimulator() -> Bool {
        #if IOS_SIMULATOR
            return true
        #else
            return false
        #endif
    }
    
    class public func hardwareString() -> String {
        var name: [Int32] = [CTL_HW, HW_MACHINE]
        var size: Int = 2
        sysctl(&name, 2, nil, &size, &name, 0)
        var hw_machine = [CChar](repeating: 0, count: Int(size))
        sysctl(&name, 2, &hw_machine, &size, &name, 0)
        
        let hardware: String = String(cString: hw_machine)
        return hardware
    }
    
    private static func getDeviceList() -> [String: Any]? {
        let bundble = Bundle.main
        if let path = bundble.path(forResource: "DeviceList", ofType: "plist") {
            return NSDictionary(contentsOfFile: path) as? [String: Any]
        }
        assertionFailure("DevicePlist.plist not found in the bundle.")
        return nil
    }
    
    class public func hardware() -> Hardware {
        let hardware = hardwareString()
        
        if hardware == "iPhone1,1" { return Hardware.IPHONE_2G }
        if hardware == "iPhone1,2" { return Hardware.IPHONE_3G }
        if hardware == "iPhone2,1" { return Hardware.IPHONE_3GS }
        
        if hardware == "iPhone3,1" { return Hardware.IPHONE_4 }
        if hardware == "iPhone3,2" { return Hardware.IPHONE_4 }
        if hardware == "iPhone3,3" { return Hardware.IPHONE_4_CDMA }
        if hardware == "iPhone4,1" { return Hardware.IPHONE_4S }
        
        if hardware == "iPhone5,1" { return Hardware.IPHONE_5 }
        if hardware == "iPhone5,2" { return Hardware.IPHONE_5_CDMA_GSM }
        if hardware == "iPhone5,3" { return Hardware.IPHONE_5C }
        if hardware == "iPhone5,4" { return Hardware.IPHONE_5C_CDMA_GSM }
        if hardware == "iPhone6,1" { return Hardware.IPHONE_5S }
        if hardware == "iPhone6,2" { return Hardware.IPHONE_5S_CDMA_GSM }
        
        if hardware == "iPhone7,1" { return Hardware.IPHONE_6_PLUS }
        if hardware == "iPhone7,2" { return Hardware.IPHONE_6 }
        if hardware == "iPhone8,2" { return Hardware.IPHONE_6S_PLUS }
        if hardware == "iPhone8,1" { return Hardware.IPHONE_6S }
        if hardware == "iPhone8,4" { return Hardware.IPHONE_SE }
        
        if hardware == "iPod1,1" { return Hardware.IPOD_TOUCH_1G }
        if hardware == "iPod2,1" { return Hardware.IPOD_TOUCH_2G }
        if hardware == "iPod3,1" { return Hardware.IPOD_TOUCH_3G }
        if hardware == "iPod4,1" { return Hardware.IPOD_TOUCH_4G }
        if hardware == "iPod5,1" { return Hardware.IPOD_TOUCH_5G }
        
        if hardware == "iPad1,1" { return Hardware.IPAD }
        if hardware == "iPad1,2" { return Hardware.IPAD_3G }
        if hardware == "iPad2,1" { return Hardware.IPAD_2_WIFI }
        if hardware == "iPad2,2" { return Hardware.IPAD_2 }
        if hardware == "iPad2,3" { return Hardware.IPAD_2_CDMA }
        if hardware == "iPad2,4" { return Hardware.IPAD_2 }
        if hardware == "iPad2,5" { return Hardware.IPAD_MINI_WIFI }
        if hardware == "iPad2,6" { return Hardware.IPAD_MINI }
        if hardware == "iPad2,7" { return Hardware.IPAD_MINI_WIFI_CDMA }
        if hardware == "iPad3,1" { return Hardware.IPAD_3_WIFI }
        if hardware == "iPad3,2" { return Hardware.IPAD_3_WIFI_CDMA }
        if hardware == "iPad3,3" { return Hardware.IPAD_3 }
        if hardware == "iPad3,4" { return Hardware.IPAD_4_WIFI }
        if hardware == "iPad3,5" { return Hardware.IPAD_4 }
        if hardware == "iPad3,6" { return Hardware.IPAD_4_GSM_CDMA }
        if hardware == "iPad4,1" { return Hardware.IPAD_AIR_WIFI }
        if hardware == "iPad4,2" { return Hardware.IPAD_AIR_WIFI_GSM }
        if hardware == "iPad4,3" { return Hardware.IPAD_AIR_WIFI_CDMA }
        if hardware == "iPad4,4" { return Hardware.IPAD_MINI_RETINA_WIFI }
        if hardware == "iPad4,5" { return Hardware.IPAD_MINI_RETINA_WIFI_CDMA }
        if hardware == "iPad4,6" { return Hardware.IPAD_MINI_RETINA_WIFI_CELLULAR_CN }
        if hardware == "iPad4,7" { return Hardware.IPAD_MINI_3_WIFI }
        if hardware == "iPad4,8" { return Hardware.IPAD_MINI_3_WIFI_CELLULAR }
        if hardware == "iPad5,1" { return Hardware.IPAD_MINI_4_WIFI }
        if hardware == "iPad5,2" { return Hardware.IPAD_MINI_4_WIFI_CELLULAR }
        if hardware == "iPad5,3" { return Hardware.IPAD_AIR_2_WIFI }
        if hardware == "iPad5,4" { return Hardware.IPAD_AIR_2_WIFI_CELLULAR }
        
        if hardware == "iPad6,3" { return Hardware.IPAD_PRO_97_WIFI }
        if hardware == "iPad6,4" { return Hardware.IPAD_PRO_97_WIFI_CELLULAR }
        if hardware == "iPad6,7" { return Hardware.IPAD_PRO_WIFI }
        if hardware == "iPad6,8" { return Hardware.IPAD_PRO_WIFI_CELLULAR }
        
        if hardware == "i386" { return Hardware.SIMULATOR }
        if hardware == "x86_64" { return Hardware.SIMULATOR }
        if hardware.hasPrefix("iPhone") { return Hardware.SIMULATOR }
        if hardware.hasPrefix("iPod") { return Hardware.SIMULATOR }
        if hardware.hasPrefix("iPad") { return Hardware.SIMULATOR }
        
        //log message that your device is not present in the list
        logMessage(hardware: hardware)
        
        return Hardware.NOT_AVAILABLE
    }
    
    class public func hardwareDescription() -> String? {
        let hardware = hardwareString()
        if let deviceList = getDeviceList() {
            let hardwareDetail = deviceList[hardware] as? [String: Any]
            if let hardwareDescription = hardwareDetail?["name"] {
                return hardwareDescription as? String
            }
        }
        //log message that your device is not present in the list
        logMessage(hardware: hardware)
        return nil
    }
    
    class public func hardwareNumber() -> Float {
        let hardware = hardwareString()
        if let deviceList = getDeviceList() {
            let hardwareDetail = deviceList[hardware] as? [String: Any]
            if let hardwareNumber = hardwareDetail?["version"] as? Float {
                return hardwareNumber
            }
        }
        //log message that your device is not present in the list
        logMessage(hardware: hardware)
        
        return 200.0 //device might be new one of missing one so returning 200.0f
    }
    
    private static func logMessage(hardware: String) {
    }
    
    func enableSpeaker(enable: Bool) {
        let audioSesstion =  AVAudioSession.sharedInstance()
        do {
            _ = try audioSesstion.overrideOutputAudioPort(enable ? AVAudioSessionPortOverride.speaker : AVAudioSessionPortOverride.none)
        } catch {
            
        }
    }
}
