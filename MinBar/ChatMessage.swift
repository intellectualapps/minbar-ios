//
//  ChatMessage.swift
//  PentorApp
//
//  Created by Segun Solaja on 5/7/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class ChatMessage: NSObject,NSCoding {

    var authorEmail:String?
    var content:String?
    var createdOn:Date?
    var messageId:String?

    override init () {
        super.init()
    }
    
    func encode(with aCoder: NSCoder){
        
        aCoder.encode(content, forKey: "content")
        aCoder.encode(createdOn, forKey: "createdOn")
        aCoder.encode(authorEmail, forKey: "authorEmail")
        aCoder.encode(messageId, forKey: "messageId")
    }
    required init(coder aDecoder: NSCoder){
        
        self.content = aDecoder.decodeObject(forKey: "content") as? String
        self.createdOn = aDecoder.decodeObject(forKey: "createdOn") as? Date
        self.authorEmail = aDecoder.decodeObject(forKey: "authorEmail") as? String
        self.messageId = aDecoder.decodeObject(forKey: "messageIds") as? String   
    }
}
