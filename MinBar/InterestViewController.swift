//
//  InterestViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/16/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import SDWebImage
import ActionSheetPicker_3_0
import APESuperHUD

class InterestViewController: BaseViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    lazy var interestView:UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    var nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.tintColor = .white
        button.setTitle("title.next".localized(), for: .normal)
        button.backgroundColor = UIColor(hex: "#2B5CAC")
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var isCreateProfileMode = true
    
    var user: User? = User.archivedUser()

    var interestData:[Interest]! = []{
        didSet{
            interestView.reloadData()
        }
    }
    
    var subInterest:[SubInterest]! = []
    var selectedSubInterest:[SubInterest]! = []
    
    var data: [String: Any]!
    var isMentorMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    func configureView(){
        self.view.backgroundColor = .white
        self.view.addSubview(interestView)
        self.view.addSubview(nextButton)
        self.setupConstraints()
        interestView.register(InterestCell.self, forCellWithReuseIdentifier: "interestCell")
        self.getInterest()
        nextButton.addTarget(self, action: #selector(showNextScreen), for: .touchUpInside)
        if (self.isMentorMode){
            nextButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "title.selectinterest".localized()
        
        
    }
    
    
    
    
    func setupConstraints(){
        interestView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        interestView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        interestView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50).isActive = true
        interestView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        
        nextButton.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        nextButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        nextButton.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
    }
    
    func getInterest(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.loading".localized(), presentingView: self.view)
        UserService.getInterest(resultCallback: { (interests) in
            DispatchQueue.main.async {
                self.interestData = interests
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                    
                })
            }
        }) { (err) in
            print(err)
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interestData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = interestView.dequeueReusableCell(withReuseIdentifier: "interestCell", for: indexPath) as! InterestCell
        let interest = self.interestData[indexPath.row]
        cell.interestNameLabel.text = interest.name
        cell.interestImage.sd_setImage(with: URL(string:interest.imageLink!), placeholderImage: UIImage(named: "background"))
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width / 3) - 5
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(5,5,0, 5)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let interest = self.interestData[indexPath.row]
        self.getSubInterest(interest: interest)
    }
    
    func getSubInterest(interest:Interest){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.loading".localized(), presentingView: self.view)
        UserService.getSubInterest(["id":interest.id!], resultCallback: { (subInterest) in
            self.subInterest = subInterest
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                
            })
            DispatchQueue.main.async {
                self.showActionSheetPicker(subInterests: subInterest)
            }
        }) { (err) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    func showActionSheetPicker(subInterests:[SubInterest]){
        let items = subInterests.map { (interest) -> String in
            return interest.name!
        }
        
        let picker = ActionSheetStringPicker(title: "title.subinterest".localized(), rows: items, initialSelection: 0, doneBlock: { (picker, index, value) in
            if(self.subInterest.count > 0){
                let subInterest = self.subInterest.filter({ (interest) -> Bool in
                    return interest.name! == value as! String
                }).first!
                if (self.isMentorMode){
                    self.findMentors(subInterestId: subInterest.subId!)
                }else{
                    APESuperHUD.showOrUpdateHUD(icon: IconType.checkMark, message: "\(subInterest.name!) Added", duration: 1, presentingView: self.view)
                    let alreadyExist = self.selectedSubInterest.filter({ (item) -> Bool in
                        return item.id! == subInterest.id!
                    }).first
                    if ( alreadyExist == .none){
                        self.selectedSubInterest.append(subInterest)
                    }
                }
            }
        }, cancel: { (picker) in
            print("cancelled")
        }, origin: self.view)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.blue], for: .normal)
        
        picker?.show()
    }
    
    func showNextScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "photoBio") as! PhotoBioViewController
        
        if let selected = selectedSubInterest{
            let interests = selected.map({ (interest) -> String in
                return interest.subId!
            }).joined(separator: ",")
            data.merge(["interest" : interests])
            // print(data["interest"])
            controller.data = data
        }else{
            controller.data = data
        }
        controller.user = user
        controller.isCreateProfileMode = isCreateProfileMode
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func findMentors(subInterestId:String){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.sending".localized(), presentingView: self.view)
        let email = user?.email ?? ""
        let params = ["email": email,"sub-interest-id":subInterestId]
        UserService.findMentors(params, resultCallback: {[weak self] (results) in
            print("Found em")
            guard let aview = self?.view else {
                return
            }
            APESuperHUD.removeHUD(animated: true, presentingView: aview, completion: { _ in
                
            })
            NotificationCenter.default.post(name: NSNotification.Name("FoundMentors"), object: nil, userInfo: ["results" : results, "sub-interest-id":subInterestId])
            DispatchQueue.main.async {
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }) { (err) in
            print(err)
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    
    
}
