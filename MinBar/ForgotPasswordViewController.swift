//
//  ForgotPasswordViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 5/21/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import APESuperHUD

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    func configureView(){
        self.navigationController?.isNavigationBarHidden = false
        sendButton.layer.cornerRadius = 4
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height:emailTextField.frame.height))
        emailTextField.leftViewMode = .always
        
        sendButton.addTarget(self, action: #selector(doForgotPasswordAction), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.makeNavigationBarTransparent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeNavigationBarTransparent(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func doForgotPasswordAction(){
        guard  let email = emailTextField.text else {
            return
        }
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Sending...", presentingView: self.view)
        let urlParam = "?email=\(email)"
        UserService.forgotPassword(urlParam: urlParam, resultCallback: {[weak self] (succcess) in

            guard let strongself = self else {
                return
            }
            APESuperHUD.showOrUpdateHUD(icon: IconType.checkMark, message: "title.sent".localized(), duration: 3, presentingView: strongself.view)
            APESuperHUD.removeHUD(animated: true, presentingView: strongself.view, completion: { _ in
                // Completed
                DispatchQueue.main.async {
                    _ = strongself.navigationController?.popToRootViewController(animated: true)
                }
            })
            
        }) { (err) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
}
