//
//  ChatViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 5/7/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import IQKeyboardManagerSwift
import PopupDialog
import APESuperHUD

class ChatViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    lazy var inputContainerView: ChatInputView = {
        let chatInputContainerView = ChatInputView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        chatInputContainerView.chatView = self
        return chatInputContainerView
    }()

    var connectionId = ""
    var userFullName = ""

    var email = ""

    var user: User? = User.archivedUser()

    let dateformatter = DateFormatter()
    var presenceHandle:UInt = 0
    var isOnline = false
    var messages = [ChatMessage]()

    deinit {
        NotificationCenter.default.removeObserver(self)
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    func configureView(){
        let barButtonItem = UIBarButtonItem(image: UIImage(named:"bin"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(deleteConnections))
        self.navigationItem.rightBarButtonItem = barButtonItem
        
        let additionButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_more_vert"),
                                                 style: UIBarButtonItemStyle.plain,
                                                 target: self,
                                                 action: #selector(choseActions))
        self.navigationItem.rightBarButtonItems = [additionButtonItem, barButtonItem]
        
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 20, right: 0)
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: "message")
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = UIColor.white
        collectionView?.keyboardDismissMode = .interactive
        setupKeyboardObservers()
        self.getMessages()
        let _email = self.email.replacingOccurrences(of: ".", with: "%")
        self.observePartnersPresence(_email)
    }
    
    func deleteConnections() {
        self.showPopup()
    }
    
    func choseActions() {
        let alert = UIAlertController(title: "title.choose".localized(), message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let blockAction = UIAlertAction(title: "button.block".localized(), style: UIAlertActionStyle.default) { (action) in
            self.blockUserConfirm()
        }
        
        alert.addAction(blockAction)
        let reportAction = UIAlertAction(title: "button.report".localized(), style: UIAlertActionStyle.default) { (action) in
            self.reportUserConfirm()
        }
        alert.addAction(reportAction)
        
        let cancel = UIAlertAction(title: "button.cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func blockUserConfirm() {
        let dialog = PopupDialog(title: "button.block".localized(),
                                 message: "block.user.message".localized())
        let buttonOne = CancelButton(title: "button.no".localized(), height: 60) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "button.yes".localized(), height: 60) {
            self.blockUser()
        }
        
        // Add buttons to dialog
        dialog.addButtons([buttonOne, buttonTwo])
        self.view.endEditing(true)
        // Present dialog
        present(dialog, animated: true, completion: nil)
    }
    
    private func blockUser() {
        let userEmail = user?.email ?? ""
        let _email = email.replacingOccurrences(of: " ", with: "")
        let connectionId = self.connectionId
        UserService.blockUser(userEmail: userEmail,
                              blockEmail: _email,
                              connectionId: connectionId,
                              resultCallback: {[weak self] (result) in
                                FirebaseConfig.connectionsRef.child(connectionId).removeValue()
                                NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
                                DispatchQueue.main.async {
                                    _ = self?.navigationController?.popToRootViewController(animated: true)
                                }
            
        }) { [weak self] (err) in
            guard let wself = self else { return }
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: wself.view)
        }
    }
    
    private func reportUserConfirm() {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        reportUser(messages: messages.map({ (chatMessage) in
            guard let email = chatMessage.authorEmail, let date = chatMessage.createdOn, let content = chatMessage.content else {
                return ""
            }
            return "<br>Email: \(email)<br>Date: \(dateFormat.string(from: date))<br>Content: \(content)"
        }))
    }
    
    private func reportUser(messages: [String]) {
        let userEmail = user?.email ?? ""
        let _email = email.replacingOccurrences(of: " ", with: "")
        UserService.reportUser(userEmail: userEmail, reportEmail: _email, connectionId: connectionId, messages: messages, resultCallback: {[weak self] (result) in
            NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
            DispatchQueue.main.async {
                _ = self?.navigationController?.popToRootViewController(animated: true)
            }
        }) { [weak self] (err) in
            guard let wself = self else { return }
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: wself.view)
        }
    }
    
    func showPopup(){
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        let dialog = PopupDialog(viewController: ratingVC,
                                 buttonAlignment: .horizontal,
                                 transitionStyle: .bounceDown,
                                 gestureDismissal: true)
        let buttonOne = CancelButton(title: "button.cancel".localized(), height: 60) {
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "button.rate".localized(), height: 60) {[weak self] in
            self?.doRateUser(Int(ratingVC.ratingView.value))
        }
        
        // Add buttons to dialog
        dialog.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(dialog, animated: true, completion: nil)
    }
    
    func doRateUser(_ rating:Int){
        self.view?.isUserInteractionEnabled = false
        self.showProgress()
        let _email = email.replacingOccurrences(of: " ", with: "")
        let param = "?user-email=\(_email)&rating=\(rating)&connection-id=\(connectionId)"
        UserService.rateUser(urlParam: param, resultCallback: {[weak self] (result) in
            self?.hideProgress()
            DispatchQueue.main.async {
                _ = self?.navigationController?.popToRootViewController(animated: true)
            }
        }) { (err) in
            self.hideProgress()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = userFullName
        IQKeyboardManager.sharedManager().enable = false
        let email = user?.email ?? ""
        let _email = email.replacingOccurrences(of: ".", with: "%")
        FirebaseConfig.connectionsRef.child(connectionId).child("participants").child(_email).setValue(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        inputContainerView.inputTextField.resignFirstResponder()
        
        let email = user?.email ?? ""
        let _email = email.replacingOccurrences(of: ".", with: "%")
        FirebaseConfig.connectionsRef.child(connectionId).child("participants").child(_email).setValue(false)
        FirebaseConfig.connectionsRef.child(connectionId).child("participants").removeObserver(withHandle: presenceHandle)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
    }
    
    
    func getMessages(){
        FirebaseConfig.connectionsRef.child(connectionId).child("messages").queryLimited(toLast: 100).observe(.childAdded, with: {[weak self] (snapshot) in
            print(snapshot.key)
            
            print(snapshot.value ?? "nothing")
            let dict = snapshot.value as? Dictionary<String,AnyObject>
            if let d = dict{
                self?.getMessageItem(dict: d)
            }
            
        })
        
    }
    
    func getMessageItem(dict:Dictionary<String,AnyObject>) {
        let chatItem = ChatMessage()
        chatItem.authorEmail = dict["author"] as? String
        chatItem.messageId = dict["id"] as? String
        chatItem.content = dict["text"] as? String
        var time = (dict["time"] as? Double) ?? 0
        if let new = dict["time"] as? Int {
            time = Double(new)
        } else if let new = dict["time"] as? String {
            time = Double(new) ?? 0
        } else {
            time = Date().timeIntervalSince1970 * 1000
        }
        chatItem.createdOn = Date(timeIntervalSince1970: time / 1000)
        messages.append(chatItem)
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.scrollToBottomAnimated(true)
        }
    }
    
    func scrollToBottomAnimated(_ animated: Bool) {
        let numberOfSections = self.collectionView?.numberOfSections ?? 0
        if numberOfSections == 0 {
            return
        }
        
        let itemsCount = self.collectionView?.numberOfItems(inSection: 0) ?? 0
        if itemsCount == 0 {
            return
        }
        let collectionBound = self.collectionView?.bounds ?? .zero
        let collectionViewContentHeight = self.collectionView?.collectionViewLayout.collectionViewContentSize.height ?? 0
        let isContentTooSmall: Bool = (collectionViewContentHeight < collectionBound.size.height)
        
        if isContentTooSmall {
            self.collectionView?.scrollRectToVisible(CGRect(x: 0, y: collectionViewContentHeight - 1, width: 1, height: 1), animated: animated)
            return
        }
        
        self.collectionView?.scrollToItem(at: NSIndexPath(item: itemsCount - 1, section: 0) as IndexPath, at: .bottom, animated: animated)
        
    }
    
    override var inputAccessoryView: UIView? {
        get {
            return inputContainerView
        }
    }
    
    func handleSend(){
        let text = self.inputContainerView.inputTextField.text ?? ""
        self.inputContainerView.inputTextField.text = ""
        let email = user?.email ?? ""
        inputContainerView.sendButton.isEnabled = false
        let ref = FirebaseConfig.connectionsRef.child(connectionId).child("messages").childByAutoId()
        let value: [String : Any] = ["author": email,
                     "id": ref.key,
                     "text": text,
                     "time": Date().toTimeStampMillis()]
        ref.setValue(value) {[weak self] (err, ref) in
            self?.inputContainerView.sendButton.isEnabled = true
        }
        if self.isOnline == false {
            self.sendPush()
        }
    }
    
    func sendPush(){
        let param = "?offline-email=\(self.email)&online-email=\(user?.email ?? "")&connection-id=\(connectionId)&platform=2"
        UserService.sendPush(urlParam: param, resultCallback: { (success) in
            print(success)
        }) { (err) in
            print(err)
        }
    }
    
    func observePartnersPresence(_ email:String){
        //child(email)
        presenceHandle = FirebaseConfig.connectionsRef.child(connectionId).child("participants").observe(.value, with: {[weak self] (snapshot) in
            if let v = snapshot.value as? Dictionary<String,Bool> {
                let isOnline = v[email]
                if let _online = isOnline {
                    self?.isOnline  = _online
                }else{
                    self?.isOnline = false
                }
            }
        })
    }
    
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return 4
        return messages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "message", for: indexPath as IndexPath) as? ChatMessageCell else {
            return UICollectionViewCell()
        }
        let message = messages[indexPath.item]
        let content = message.content ?? ""
        let size = estimateFrameForText(content)
        let email = user?.email ?? ""
        let authorEmail = message.authorEmail ?? ""
        if authorEmail != email && email.characters.count > 0 {
            cell.bubbleViewLeftAnchor?.isActive = true
            cell.bubbleViewRightAnchor?.isActive = false
            
            cell.timeStampLabelLeftAnchor?.isActive = true
            cell.timeStampLabelRightAnchor?.isActive = false
            cell.bubbleView.backgroundColor = UIColor(hex: "#2B5CAC")
            cell.textView.textColor = .white
            //cell.textView.textAlignment = .left
            cell.timeStampLabel.textColor = .white
            cell.timeStampLabel.textAlignment = .left
            
        } else {
            cell.bubbleViewLeftAnchor?.isActive = false
            cell.bubbleViewRightAnchor?.isActive = true
            
            cell.timeStampLabelLeftAnchor?.isActive = false
            cell.timeStampLabelRightAnchor?.isActive = true
            cell.bubbleView.backgroundColor = UIColor(hex: "#F2F2F2")
            cell.textView.textColor = .darkGray
            // cell.textView.textAlignment = .right
            cell.timeStampLabel.textColor = .lightGray
            cell.timeStampLabel.textAlignment = .right
            
        }
        cell.bubbleWidthAnchor?.constant = size.width + 40
        if let date = message.createdOn {
            dateformatter.dateFormat = "hh:mm a"
            cell.timeStampLabel.text = dateformatter.string(from: date)
        }
        cell.textView.text = message.content
        
        return cell
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]
        let content = message.content ?? ""
        height = estimateFrameForText(content).height + 52
        let width = UIScreen.main.bounds.width
        return CGSize(width: width, height: height)
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
    }
    func handleKeyboardDidShow() {
        
        if messages.count > 0 {
            let indexPath = IndexPath(item: messages.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    fileprivate func estimateFrameForText(_ text: String) -> CGRect {
        let size = CGSize(width: 300, height: 1000)
        let font = UIFont(name: "Avenir", size: 16) ?? UIFont.systemFont(ofSize: 16)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: font], context: nil)
    }
}
