//
//  BasicInfoViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/5/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import SwiftValidator
import APESuperHUD

class BasicInfoViewController: BaseViewController,ValidationDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    @IBOutlet weak var firstNameLabel: UITextField!
    
    @IBOutlet weak var lastNameTextfield: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    
    @IBOutlet weak var birthdayTextField: UITextField!
    
    @IBOutlet weak var mentorSwitch: UISwitch!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet  var containerViews:[UIView]!
    var genderPicker:UIPickerView!
    let genders = ["gender.male".localized(),"gender.female".localized()]
    let dateFormatter:DateFormatter = DateFormatter()
    var selectedGender:String!
    
    var user: User? = User.archivedUser()
    var isCreateProfileMode = true
    
    var selectedDate = Date()
    let validator = Validator()

    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    func configureView() {
        containerViews.forEach { (view) in
            view.layer.borderWidth = 2
            view.layer.borderColor = UIColor(hex:"#F5F5F5").cgColor
            view.layer.cornerRadius = 4
        }
        nextButton.addTarget(self, action: #selector(doValidate), for: .touchUpInside)
        phoneNumberTextField.keyboardType = .numberPad
        let datePicker:UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        self.birthdayTextField.inputView =  datePicker
        
        genderPicker = UIPickerView()
        genderPicker.autoresizingMask  = [.flexibleWidth , .flexibleHeight]
        genderPicker.showsSelectionIndicator = true
        genderPicker.delegate = self
        genderPicker.dataSource = self
        genderPicker.tag = 11
        
        genderTextField.inputView = genderPicker
        genderTextField.delegate = self
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = isCreateProfileMode == true ? "Basic Info" :"Profile Info"
        if isCreateProfileMode == false {
            self.getUser()
        }
    }
    
    func registerValidation(){
        
        validator.styleTransformers(success: { (validationRule) -> Void in
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0
                
            }
            
        }, error:{ (validationError) -> Void in
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 2.0
            }
        })
        validator.registerField(firstNameLabel, errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(lastNameTextfield, errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(phoneNumberTextField, errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(birthdayTextField, errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(genderTextField, errorLabel: nil, rules: [RequiredRule()])
        
    }
    
    func validationSuccessful() {
        guard let firstname = firstNameLabel.text ,
            let lastName = lastNameTextfield.text ,
            let phoneNumber = phoneNumberTextField.text,
            let birthday = birthdayTextField.text else {
                return
        }
        let isMentor:Bool =  mentorSwitch.isOn
        let dataDict: [String: Any] = ["firstname":firstname,
                                       "lastname":lastName,
                                       "phonenumber":phoneNumber,
                                       "gender":selectedGender,
                                       "birthday":birthday ,
                                       "is_mentor":isMentor]
        self.showNextScreen(data: dataDict)
    }
    
    
    func dateChanged(_ datePicker:UIDatePicker){
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let s = dateFormatter.string(from: datePicker.date)
        
        self.selectedDate = datePicker.date
        self.birthdayTextField.text = s
    }
    
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let gender = genders[row]
        selectedGender = gender.lowercased() == "female" ? "f" :"m"
        self.genderTextField.text = genders[row]
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == genderTextField){
            genderTextField.text = genders[0]
            let gender = genders[0]
            selectedGender = (gender.lowercased() == "female") ? "f" :"m"
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resourcesd that can be recreated.
    }
    
    func doValidate(){
        validator.validate(self)
    }
    
    func prefillFields(user:User){
        self.firstNameLabel.text = user.firstName
        self.lastNameTextfield.text = user.lastName
        self.phoneNumberTextField.text = user.phoneNumber
        if let g = user.gender{
            self.genderTextField.text = g.capitalized
            selectedGender  = (g.lowercased() == "male") ? "m"  : "f"
        }
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if let birthday  = user.birthday{
            let date = dateFormatter.string(from: birthday)
            self.birthdayTextField.text = date
        }
        self.mentorSwitch.setOn(user.isMentor!, animated: true)
    }
    func getUser(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Please wait...", presentingView: self.view)
        let email = user?.email ?? ""
        let param = ["email": email]
        UserService.getUser(param: param, resultCallback: {[weak self] (result) in
            guard let aview = self?.view else {
                return
            }
            APESuperHUD.removeHUD(animated: true, presentingView: aview, completion: { _ in
                // Completed
                DispatchQueue.main.async {
                    self?.user = result
                    self?.prefillFields(user: result)
                }
            })
            
        }) { (err) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    func showNextScreen(data:[String: Any]) {
        if ( mentorSwitch.isOn) {
            let controller = InterestViewController()
            controller.isCreateProfileMode = isCreateProfileMode
            controller.data = data
            controller.user = user
            self.navigationItem.title = ""
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "photoBio") as? PhotoBioViewController {
                controller.data = data
                controller.user = user
                controller.isCreateProfileMode = isCreateProfileMode
                self.navigationItem.title = ""
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
