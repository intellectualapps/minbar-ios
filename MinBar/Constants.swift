//
//  Constants.swift
//  Scry
//
//  Created by Segun Solaja on 10/20/16.
//  Copyright © 2016 Scry. All rights reserved.
//

import UIKit


func getDateFromTimestamp(_ timestamp:String) -> Date {
    let interval = TimeInterval(timestamp) ?? 0
    return Date(timeIntervalSince1970: interval / 1000)
    
}

class Constants: NSObject {
    static let user: User? = User.archivedUser()
    
    class func baseURL () ->String{
        return "https://minbarapp-ios.appspot.com/api/v1"
    }
    
    
    class func relativeURL() -> Dictionary<String, String> {
        return [
            "SIGN_UP" :"/user",
            "LOGIN" :"/user/authenticate",
            "UPDATE_PROFILE":"/user/basic",
            "INTEREST":"/interest",
            "SUB_INTEREST":"/interest/sub",
            "MENTOR_CONNECTIONS":"/mentor/connection/:email",
            "USER_DEVICEPUSHTOKEN":"/user/device-token/",
            "ACCEPT_OR_DECLINE_CONNECTIONS":"/mentor/connection",
            "SEND_PUSH":"/mentor/connection/notify",
            "FORGOT_PASSWORD":"/user/password",
            "RATE_USER":"/user/rate",
            "MENTORS":"/mentor"
        ]
    }
    
    class func headerConstants(_ headerParam:Dictionary<String,String>)->Dictionary<String,String> {
        var defaultsDict: Dictionary<String,String>  =  [:]
        if let token = user?.authToken {
            defaultsDict["Authorization"] = "Bearer: \(token)"
        }
        if headerParam.isEmpty {
            return defaultsDict
        } else {
            defaultsDict.merge(headerParam)
            return defaultsDict
        }
    }
}
