//
//  ProfileViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/5/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class ProfileViewController: UITableViewController,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    func configureView(){
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func showFindMentor(){
        //self.performSegue(withIdentifier: "findMentor", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "title.profile".localized()
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "man")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let font = UIFont(name: "Avenir-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20)
        let attribute = [NSFontAttributeName: font]
        return NSAttributedString(string: "title.setupprofile".localized(), attributes: attribute)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let font =  UIFont(name: "Avenir", size: 15) ?? UIFont.systemFont(ofSize: 15)
        let attribute = [NSFontAttributeName: font]
        return NSAttributedString(string: "message.need.setupprofile".localized(), attributes: attribute)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let font =  UIFont(name: "Avenir-Medium", size: 15) ?? UIFont.systemFont(ofSize: 15)
        let attribute = [NSFontAttributeName: font,
                         NSForegroundColorAttributeName:UIColor(hex:"#168BFE")]
        return NSAttributedString(string: "title.createprofile".localized(), attributes: attribute)
    }
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -64
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "basicInfo") as? BasicInfoViewController {
            self.navigationItem.title = ""
            self.navigationController?.pushViewController(controller, animated: true)
            print("Button tapped")
        }
    }
    
    deinit {
        print(NSStringFromClass(self.classForCoder) + "." + #function)
        NotificationCenter.default.removeObserver(self)
    }
}
