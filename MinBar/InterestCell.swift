//
//  InterestCell.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/16/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class InterestCell: UICollectionViewCell {
    let interestImage:UIImageView = {
        let imageV = UIImageView()
        imageV.contentMode = .scaleAspectFill
        imageV.clipsToBounds = true
        imageV.image = UIImage(named: "background")
        imageV.translatesAutoresizingMaskIntoConstraints = false
        return imageV
    }()
    
    let interestImageOverlay:UIView = {
        let imageV = UIView()
        imageV.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        imageV.translatesAutoresizingMaskIntoConstraints = false
        return imageV
    }()
    
    let interestNameLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "title.sport".localized()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir-Medium", size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let checkImage:UIImageView = {
        let imageV = UIImageView()
        imageV.isHidden = true
        imageV.contentMode = .scaleAspectFill
        imageV.clipsToBounds = true
        imageV.translatesAutoresizingMaskIntoConstraints = false
        return imageV
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(interestImage)
        self.addSubview(interestImageOverlay)
        self.addSubview(interestNameLabel)
        self.addSubview(checkImage)
        self.setupConstraints()
    }
    
    
    func setupConstraints(){
        interestImage.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        interestImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
        interestImage.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        interestImage.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        interestImageOverlay.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        interestImageOverlay.topAnchor.constraint(equalTo: topAnchor).isActive = true
        interestImageOverlay.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        interestImageOverlay.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        
        interestNameLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        interestNameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        interestNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        interestNameLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        checkImage.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        checkImage.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        checkImage.heightAnchor.constraint(equalToConstant: 30).isActive = true
        checkImage.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
