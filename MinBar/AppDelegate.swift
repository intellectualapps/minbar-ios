//
//  AppDelegate.swift
//  PentorApp
//
//  Created by Segun Solaja on 1/18/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Fabric
import Crashlytics
import TwitterKit
import IQKeyboardManagerSwift
import Firebase
import SASlideMenu
import FirebaseDatabase



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var pushToken:String! = ""
    class func sharedAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    override init() {
        super.init()
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        Database.setLoggingEnabled(true)
    }
    let requestIdentifier = "SampleRequest"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        UIApplication.shared.statusBarStyle = .lightContent
        let font = UIFont(name: "Avenir-Heavy", size: 18) ?? UIFont.systemFont(ofSize: 18)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName: font]
        UIBarButtonItem.appearance().tintColor =  UIColor.white
        UINavigationBar.appearance().tintColor =  UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(hex: "#2B5CAC")
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(),for: .default)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Fabric.with([Crashlytics.self])
        Twitter.sharedInstance().start(withConsumerKey: "4mOm47EZrYkYKl8hTW1wl97Tm", consumerSecret: "a62zCUe7fXUVTgLZRzFc9EoaiIhuO4V3nVb7LXLM4xbAoiG5l7")
        self.initializeNotificationServices()
        
        return true
    }
    
    //  func sendLocalNotification(){
    //    let content = UNMutableNotificationContent()
    //    content.title = "Intro to Notifications"
    //    content.subtitle = "Lets code,Talk is cheap"
    //    content.body = "Sample code from WWDC"
    //    content.sound = UNNotificationSound.default()
    //
    //
    //    let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5.0, repeats: false)
    //    let request = UNNotificationRequest(identifier:requestIdentifier, content: content, trigger: trigger)
    //    UNUserNotificationCenter.current().delegate = self
    //    UNUserNotificationCenter.current().add(request){(error) in
    //
    //      if (error != nil){
    //
    //        print(error?.localizedDescription ?? "nil")
    //      }
    //    }
    //  }
    
    //  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //
    //    print("Tapped in notification")
    //  }
    
    //  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //
    //    print("Notification being triggered")
    //    //You can either present alert ,sound or increase badge while the app is in foreground too with ios 10
    //    //to distinguish between notifications
    //    if notification.request.identifier == requestIdentifier{
    //      let message = ["notification-type":"connection-accepted","mentee-email":"solejay@yahoo.com","photo-url": "photoUrl","connection-id": "123"]
    //      remoteMessageRecieved(message)
    //      completionHandler( [.alert,.sound,.badge])
    //
    //    }
    //  }
    
    func initializeNotificationServices() -> Void {
        let types: UIUserNotificationType = [UIUserNotificationType.badge,UIUserNotificationType.sound,UIUserNotificationType.alert]
        let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: types, categories: nil)
        UIApplication.shared.registerForRemoteNotifications()
        UIApplication.shared.registerUserNotificationSettings(settings)
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        pushToken = convertDeviceTokenToString(deviceToken as NSData)
        print("token =\(pushToken)")
        // ...register device token with our Time Entry API server via REST
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Device token for push notifications: FAIL -- ")
        print(error)
    }
    
    fileprivate func convertDeviceTokenToString(_ deviceToken:NSData) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        
        var deviceTokenStr = deviceToken.description.replacingOccurrences(of: ">", with: "")
        deviceTokenStr = deviceTokenStr.replacingOccurrences(of:"<", with: "")
        deviceTokenStr = deviceTokenStr.replacingOccurrences(of:" ", with: "")
        
        // Our API returns token in all uppercase, regardless how it was originally sent.
        // To make the two consistent, I am uppercasing the token string here.
        //deviceTokenStr = deviceTokenStr.uppercaseString
        return deviceTokenStr
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print(userInfo)
        UIApplication.shared.applicationIconBadgeNumber = 0
        if(UIApplication.shared.applicationState  == UIApplicationState.active){
            self.remoteMessageRecieved(userInfo, inForegroundMode: true)
        }else{
            self.remoteMessageRecieved(userInfo, inForegroundMode: false)
        }
        
    }
    
    func getRootController() -> SASlideMenuRootViewController{
        let _rootController = self.window?.rootViewController as? SASlideMenuRootViewController
        var rootController:SASlideMenuRootViewController!
        if let r = _rootController{
            rootController = r
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            rootController = storyboard.instantiateViewController(withIdentifier: "home") as! SASlideMenuRootViewController
            //rootController.configure()
            self.window?.rootViewController = rootController
        }
        
        return rootController
    }
    
    func remoteMessageRecieved(_ userInfo:[AnyHashable: Any], inForegroundMode:Bool = false){
        let type = (userInfo["notification-type"] as? String) ?? ""
        let rootController = getRootController()
        if(inForegroundMode == true){
            
        }else{
            switch type {
            case "connection-request":
                let mentee_email  = (userInfo["mentee-email"] as? String) ?? ""
                let photo_url  = (userInfo["photo-url"] as? String) ?? ""
                let connection_id  = (userInfo["connection-id"] as? String) ?? ""
                
                print("\(mentee_email) -- \(photo_url) -- \(connection_id)")
                rootController.leftMenu.selectContent(at: IndexPath(row:0,section:0), scrollPosition: UITableViewScrollPosition.bottom)
                NotificationCenter.default.post(name: NSNotification.Name("connectionRequest"), object: nil)
                
            case "connection-accepted":
                let mentee_email  = (userInfo["mentee-email"] as? String) ?? ""
                let photo_url  = (userInfo["photo-url"] as? String) ?? ""
                let connection_id  = (userInfo["connection-id"] as? String) ?? ""
                
                print("\(mentee_email) -- \(photo_url) -- \(connection_id)")
                rootController.leftMenu.selectContent(at: IndexPath(row:0,section:0), scrollPosition: UITableViewScrollPosition.bottom)
            case "connection-declined":
                let mentee_email  = (userInfo["mentee-email"] as? String) ?? ""
                let photo_url  = (userInfo["photo-url"] as? String) ?? ""
                let connection_id  = (userInfo["connection-id"] as? String) ?? ""
                
                print("\(mentee_email) -- \(photo_url) -- \(connection_id)")
                rootController.leftMenu.selectContent(at: IndexPath(row:0,section:0), scrollPosition: UITableViewScrollPosition.bottom)
            case "offline-user":
                
                let photo_url  = (userInfo["photo-url"] as? String) ?? ""
                let connection_id  = (userInfo["connection-id"] as? String) ?? ""
                
                print("\(photo_url) -- \(connection_id)")
                rootController.leftMenu.selectContent(at: IndexPath(row:0,section:0), scrollPosition: UITableViewScrollPosition.bottom)
            default:
                break
            }
        }
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        
        if(FBSDKApplicationDelegate.sharedInstance().application(app, open: url as URL!, sourceApplication: sourceApplication, annotation: nil)){
            return true
        }else{
            return Twitter.sharedInstance().application(app, open: url, options: options)
        }
    }
    
    
}

