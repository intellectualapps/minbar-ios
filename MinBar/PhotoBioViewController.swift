//
//  PhotoBioViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/9/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import MobileCoreServices
import Alamofire
import APESuperHUD
import SwiftValidator


class PhotoBioViewController: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,ValidationDelegate {
    @IBOutlet weak var itemHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var addPhotoButton: MButton!
    @IBOutlet weak var photoView: UIImageView!
    
    @IBOutlet weak var aboutYouTextView: UITextView!
    
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var locationTextField: UITextField!
    var data: [String: Any]!
    let validator = Validator()
    
    var selectedImageBase64:String?
    let PLACES_API_KEY = "AIzaSyAxH5jmqfo0AuYynnZdt0I58YLM9aUueUM"
    let PLACES_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    let REVERSE_PLACES_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json"
    var autoCompleteTableView: UITableView!
    var selectedLongtitude:Double?
    var selectedLatitude:Double?
    var user: User? = User.archivedUser()
    
    var isCreateProfileMode = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.configureView()
    }
    var searchResults: [String] = [] {
        didSet{
            self.autoCompleteTableView.reloadData()
        }
    }
    func configureView(){
        addPhotoButton.extendedEdges = UIEdgeInsets(top: 70, left: 70, bottom: 0, right: 0)
        addPhotoButton.addTarget(self, action: #selector(photoViewTapped), for: .touchUpInside)
        locationTextField.delegate = self
        photoView.layer.cornerRadius = photoView.frame.width / 2
        photoView.clipsToBounds = true
        autoCompleteTableView = UITableView()
        // autoCompleteTableView.frame = CGRect(x:0, y:60, width:self.locationTextField.frame.size.width, height:200)
        autoCompleteTableView.dataSource = self
        autoCompleteTableView.delegate = self
        autoCompleteTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        submitButton.addTarget(self, action: #selector(updateProfileAction), for: .touchUpInside)
        self.view.addSubview(autoCompleteTableView)
        self.registerValidation()
        if (isCreateProfileMode == false){
            self.prefillValues()
        }
    }
    
    func registerValidation(){
        
        validator.styleTransformers(success: { (validationRule) -> Void in
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0
                
            }
            
        }, error:{ (validationError) -> Void in
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        validator.registerField(locationTextField, errorLabel: nil, rules: [RequiredRule()])
        
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPAD {
            itemHeightConstraint?.constant = 80
        } else {
            itemHeightConstraint?.constant = 100
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "title.photo_bio".localized()
    }
    func isCameraAvailable() ->Bool{
        return UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
    }
    func isPhotoLibraryAvailable() ->Bool{
        return UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
    }
    
    func photoViewTapped(){
        self.view.endEditing(true)
        let actionSheet = UIAlertController(title: "title.upload".localized(), message: "", preferredStyle: .actionSheet)
        let cameraController:UIImagePickerController = UIImagePickerController()
        cameraController.delegate = self
        cameraController.mediaTypes = [kUTTypeImage as String]
        cameraController.allowsEditing = true
        
        let choosePhotoAction  = UIAlertAction(title: "title.choose.photo".localized(), style: UIAlertActionStyle.default) { (action) in
            if(self.isPhotoLibraryAvailable()){
                cameraController.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(cameraController, animated: true, completion: nil)
            }
        }
        let takePhotoAction  = UIAlertAction(title: "title.takephoto".localized(), style: UIAlertActionStyle.default) { (action) in
            if(self.isCameraAvailable()){
                cameraController.sourceType = UIImagePickerControllerSourceType.camera
                self.present(cameraController, animated: true, completion: nil)
            }
            
        }
        let cancelAction  = UIAlertAction(title: "button.cancel".localized(), style: UIAlertActionStyle.cancel) { (action) in
        }
        
        actionSheet.addAction(choosePhotoAction)
        actionSheet.addAction(takePhotoAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaInfo:String = info[UIImagePickerControllerMediaType] as! String
        if(mediaInfo as CFString  == kUTTypeImage){
            photoView.image = info[UIImagePickerControllerEditedImage] as? UIImage
            
            self.selectedImageBase64 = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage, 1.0)?.base64EncodedString()
            
            photoView.contentMode = UIViewContentMode.scaleAspectFill
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text!.characters.count > 0){
            let y = self.locationTextField.convert(self.locationTextField.frame, to: self.view)
            let yCoordinate = y.origin.y  -  250
            autoCompleteTableView.frame = CGRect(x:16, y:yCoordinate, width:self.locationTextField.frame.size.width, height:200)
            autoCompleteTableView.isHidden = false
            self.fetchPredictions(theString: textField.text!)
            
        }
        
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text! == "") {
            autoCompleteTableView.isHidden = true
        }
    }
    
    
    func fetchPredictions(theString:String){
        Alamofire.request(PLACES_URL,method: .get, parameters: ["input" : theString,"key":PLACES_API_KEY]).responseJSON { (res) -> Void in
            if(res.result.isSuccess){
                let predictionDict = res.result.value as! Dictionary<String,AnyObject>
                let predictArray = predictionDict["predictions"] as! [Dictionary<String,AnyObject>]
                print(predictArray)
                self.searchResults.removeAll()
                for dict:Dictionary<String,AnyObject> in predictArray {
                    for (dictkey,_) in dict{
                        if (dictkey == "description")
                        {
                            let desc = dict[dictkey] as! String
                            print(desc)
                            self.searchResults.append(desc)
                        }
                        
                    }
                    
                }
                
                self.autoCompleteTableView.reloadData()
            }else{
                print(res.result.error.debugDescription)
            }
            
            
        }
        
    }
    func findLocation(theString:String){
        Alamofire.request(REVERSE_PLACES_URL,method: .get , parameters: ["query":theString,"key":PLACES_API_KEY]).responseJSON { (res) -> Void in
            
            if(res.result.isSuccess){
                
                let result = res.result.value as! Dictionary<String,AnyObject>
                
                let resultLocations = result["results"] as! [Dictionary<String,AnyObject>]
                
                if(resultLocations.count > 0){
                    let location = resultLocations.first! as Dictionary<String,AnyObject>
                    
                    
                    let longitude = ((location["geometry"]! as! Dictionary<String,AnyObject>)["location"]! as! Dictionary<String,AnyObject>)["lng"] as! Double
                    let latitude = ((location["geometry"]! as! Dictionary<String,AnyObject>)["location"]! as! Dictionary<String,AnyObject>)["lat"] as! Double
                    print("\(longitude)  \(latitude)")
                    self.selectedLongtitude = longitude
                    self.selectedLatitude = latitude
                }
            }else{
                // print(res.result.error)
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = autoCompleteTableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        let result  = searchResults[indexPath.row]
        cell.textLabel?.text = result
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result  = searchResults[indexPath.row]
        self.locationTextField.text = result
        self.findLocation(theString: result)
        autoCompleteTableView.isHidden = true
        
    }
    
    func validationSuccessful() {
        self.doUpdateProfileAction()
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    func updateProfileAction(){
        validator.validate(self)
    }
    func prefillValues(){
        self.aboutYouTextView.text = user?.shortBio
        self.detailTextView.text = user?.longBio
        self.locationTextField.text = user?.address
        self.selectedLatitude = user?.latitude
        self.selectedLongtitude = user?.longitude
        if let profileURL = user?.profilePhotoURL {
            self.setProfileImage(str:profileURL , imageView: self.photoView)
        }
        
    }
    
    func doUpdateProfileAction(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.pleasewait".localized(), presentingView: self.view)
        var bodyParams = [String: Any]()
        bodyParams["email"] = user?.email ?? ""
        bodyParams["first-name"] = (data["firstname"] as? String) ?? ""
        bodyParams["last-name"] = (data["lastname"] as? String) ?? ""
        bodyParams["birthday"] = (data["birthday"] as? String) ?? ""
        bodyParams["phone-number"] = (data["phonenumber"] as? String) ?? ""
        bodyParams["is-mentor"] = String((data["is_mentor"] as? Bool) ?? false)
        bodyParams["short-bio"] = aboutYouTextView?.text ?? ""
        bodyParams["long-bio"] = detailTextView?.text ?? ""
        bodyParams["lon"] = String((selectedLongtitude ?? 0))
        bodyParams["lat"] = String((selectedLatitude ?? 0))
        bodyParams["address"] = self.locationTextField?.text ?? ""
        bodyParams["gender"] = (data["gender"] as? String) ?? ""
        if let image =  selectedImageBase64{
            bodyParams.merge(["image-data":image,"image-type":"pp"])
        }
        
        if let interest = data["interest"] as? String{
            print(interest)
            bodyParams.merge(["mentor-interests":interest])
        }
        
        UserService.updateProfile(bodyParams, resultCallback: { (user) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.happyFace, message: "Profile Updated", duration: 3, presentingView: self.view)
            NotificationCenter.default.post(name: NSNotification.Name("RefreshProfile"), object: nil)
            self.perform(#selector(PhotoBioViewController.dismissView), with: nil, afterDelay: 3)
            print(user.profilePhotoURL ?? "title.none".localized())
        }) { (err) in
            print(err)
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    func dismissView(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setProfileImage(str:String,imageView:UIImageView){
        if (str.contains("pentor-bff")){
            Alamofire.request(str, method: .get, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (response) in
                if response.result.isSuccess {
                    let result = response.result.value as! Dictionary<String,AnyObject>
                    let base64 = result["base64ImageString"] as! String
                    let data  = Data(base64Encoded: base64, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                    DispatchQueue.main.async {
                        self.selectedImageBase64 =  UIImageJPEGRepresentation(UIImage(data: data!)!, 1.0)?.base64EncodedString()
                        imageView.image = UIImage(data: data!)
                    }
                }else{
                    print(response.result.error!.localizedDescription)
                    print("error")
                }
            }
        }else{
            imageView.sd_setImage(with: URL(string:str)!, placeholderImage: UIImage(named:""))
        }
    }
}
