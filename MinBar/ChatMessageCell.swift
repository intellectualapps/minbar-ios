//
//  ChatMessageCell.swift
//  PentorApp
//
//  Created by Segun Solaja on 5/7/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class ChatMessageCell: UICollectionViewCell {
    
    let bubbleView: UIView = {
        let theView = UIView()
        theView.backgroundColor = UIColor(hex: "#2B5CAC")
        theView.translatesAutoresizingMaskIntoConstraints = false
        theView.layer.cornerRadius = 8
        theView.layer.masksToBounds = true
        return theView
    }()
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = "update.message".localized()
        tv.font = UIFont(name: "Avenir", size: 16)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = UIColor.clear
        tv.textContainerInset = UIEdgeInsets.zero
        tv.textColor = .white
        tv.dataDetectorTypes = UIDataDetectorTypes.all
        tv.isEditable = false
        tv.isUserInteractionEnabled = true
        return tv
    }()
    
    let timeStampLabel: UILabel = {
        let label = UILabel()
        label.text = "12:00 AM"
        label.font = UIFont(name: "Avenir", size: 12)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var bubbleWidthAnchor: NSLayoutConstraint?
    var bubbleViewRightAnchor: NSLayoutConstraint?
    var bubbleViewLeftAnchor: NSLayoutConstraint?
    var timeStampLabelRightAnchor: NSLayoutConstraint?
    var timeStampLabelLeftAnchor: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(bubbleView)
        addSubview(textView)
        addSubview(timeStampLabel)
        
        setupConstraints()
    }
    
    func setupConstraints() {
        
        bubbleViewRightAnchor = bubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
        bubbleViewLeftAnchor = bubbleView.leftAnchor.constraint(equalTo:self.leftAnchor, constant: 8)
        bubbleViewLeftAnchor?.isActive = true
        bubbleView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        bubbleWidthAnchor = bubbleView.widthAnchor.constraint(equalToConstant: 300)
        bubbleWidthAnchor?.isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant:-10).isActive = true
        
        timeStampLabelLeftAnchor = timeStampLabel.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8)
        timeStampLabelLeftAnchor?.isActive = true
        timeStampLabelRightAnchor = timeStampLabel.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8)
        timeStampLabel.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant:-4).isActive = true
        timeStampLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        
        textView.leftAnchor.constraint(equalTo: bubbleView.leftAnchor, constant: 8).isActive = true
        textView.topAnchor.constraint(equalTo: bubbleView.topAnchor,constant: 8).isActive = true
        textView.rightAnchor.constraint(equalTo: bubbleView.rightAnchor, constant: -8).isActive = true
        textView.bottomAnchor.constraint(equalTo: timeStampLabel.topAnchor).isActive = true
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
