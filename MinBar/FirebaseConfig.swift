//
//  FirebaseConfig.swift
//  PentorApp
//
//  Created by Segun Solaja on 5/7/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class FirebaseConfig: NSObject {
  
  static let connectionsRef:DatabaseReference = {
    let ref =  Database.database().reference().child("connections")
    ref.keepSynced(true)
    return ref
  }()
  
}
