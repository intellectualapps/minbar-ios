//
//  ViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 1/18/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SASlideMenu
import Fabric
import TwitterKit
import APESuperHUD
import SwiftValidator


class ViewController: UIViewController,ValidationDelegate {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var privacyBtn: LinkableButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var linkedInButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    let validator = Validator()
    var isSignUp:Bool = true
    
    var user: User? = User.archivedUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        privacyBtn?.extendedEdges = UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20)
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
        self.registerValidation()
        UIApplication.shared.isStatusBarHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = user{
            self.showHomeScreen(false)
        }
    }
    
    
    func registerValidation(){
        
        validator.styleTransformers(success: { (validationRule) -> Void in
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0
                
            }
            
        }, error:{ (validationError) -> Void in
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let _ = validationError.field as? UITextField {
                //textField.layer.borderColor = UIColor.red.cgColor
                //textField.layer.borderWidth = 2.0
                APESuperHUD.showOrUpdateHUD(icon: IconType.info,
                                            message: "invalid.email_password".localized(),
                                            duration: 3,
                                            presentingView: self.view)
            }
        })
        validator.registerField(emailTextField, errorLabel: nil,
                                rules: [RequiredRule(),
                                        EmailRule(message: "Invalid email")])
        validator.registerField(passwordTextField, errorLabel: nil, rules: [RequiredRule()])
        
    }
    func validationSuccessful() {
        if (isSignUp){
            self.signUpAction()
        }else{
            self.loginAction()
        }
    }
    
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    func signUpAction(){
        self.view?.endEditing(true)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.pleasewait".localized(), presentingView: self.view)
        guard let email = emailTextField.text, let password = passwordTextField.text else{return}
        
        UserService.signUp(type: .email, email: email, password: password, resultCallback: { (result) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                // Completed
            })
            self.showHomeScreen()
        }) { (err) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    func loginAction(){
        self.view?.endEditing(true)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.pleasewait".localized(), presentingView: self.view)
        guard let email = emailTextField.text, let password = passwordTextField.text else{return}
        
        UserService.login(email: email, password: password, type: .email, resultCallback: { (result) in
            APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                // Completed
            })
            self.showHomeScreen()
        }) { (err) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    
    func configureView(){
        self.navigationController?.isNavigationBarHidden = true
        signInButton.layer.cornerRadius  = 4
        // registerButton.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        // registerButton.layer.borderWidth = 0.5
        registerButton.layer.cornerRadius  = 4
        
        let emailBorder = UIView(frame: CGRect(x: 0,
                                               y:emailTextField.frame.height - 1 ,
                                               width: emailTextField.frame.width,
                                               height: 1))
        emailBorder.backgroundColor = .white
        emailTextField.delegate = self
        emailTextField.addSubview(emailBorder)
        
        let passwordBorder = UIView(frame: CGRect(x: 0,
                                                  y:passwordTextField.frame.height - 1 ,
                                                  width: passwordTextField.frame.width,
                                                  height: 1))
        passwordBorder.backgroundColor = .white
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
        passwordTextField.addSubview(passwordBorder)
        
        facebookButton.layer.cornerRadius = 4
        twitterButton.layer.cornerRadius = 4
        // linkedInButton.layer.cornerRadius = 4
        
        registerButton.addTarget(self, action: #selector(signUpButtonTapped), for: .touchUpInside)
        facebookButton.addTarget(self, action: #selector(facebookSignUp), for: .touchUpInside)
        twitterButton.addTarget(self, action: #selector(twitterSignUp), for: .touchUpInside)
        signInButton.addTarget(self, action:  #selector(loginButtonTapped), for: .touchUpInside)
        forgotPasswordButton.addTarget(self, action:  #selector(forgotPasswordAction), for: .touchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signUpButtonTapped(){
        isSignUp = true
        validator.validate(self)
    }
    func loginButtonTapped(){
        isSignUp = false
        validator.validate(self)
    }
    
    func showHomeScreen(_ animated:Bool = true){
        DispatchQueue.main.async {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "home") as? SASlideMenuRootViewController {
                self.present(controller, animated: animated, completion: nil)
            }
        }
    }
    
    func forgotPasswordAction(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "forgotPassword") as? ForgotPasswordViewController {
            self.navigationItem.title = ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func twitterSignUp(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.pleasewait".localized(), presentingView: self.view)
        Twitter.sharedInstance().logIn { (session, error) in
            if (session != .none) {
                print("signed in as \(String(describing: session?.userName))");
                self.fetchTwitterEmail()
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                    // Completed
                })
            }
        }
    }
    func fetchTwitterEmail(){
        let  client = TWTRAPIClient.withCurrentUser()
        //  let request = client.urlRequest(withMethod: "GET", url: "https://api.twitter.com/1.1/account/verify_credentials.json", parameters: ["include_email": "true", "skip_status": "true"], error: nil)
        client.requestEmail { (email, err) in
            if let _email = email{
                UserService.signUp(type: .twitter, email: _email, password: "na", resultCallback: { (result) in
                    APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                        // Completed
                    })
                    self.showHomeScreen()
                }) { (err) in
                    APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
                }
            }else{
                APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: "Could not access your email this time. Please try again ", duration: 3, presentingView: self.view)
            }
        }
        
    }
    
    func facebookSignUp(){
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.pleasewait".localized(), presentingView: self.view)
        let fbSignIn:FBSDKLoginManager = FBSDKLoginManager()
        //,"user_mobile_phone"
        fbSignIn.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            if((error) != nil){
                APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace,
                                            message: "An Error ocurred while trying to sign in, please try again ",
                                            duration: 3,
                                            presentingView: self.view)
            } else if result?.isCancelled == true {
                APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace,
                                            message: "You have cancelled access to your facebook profile, please allow access to sign in to Pentor",
                                            duration: 3,
                                            presentingView: self.view)
            }else if result?.declinedPermissions.contains("public_profile") == true {
                APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace,
                                            message: "You have denied permission to your facebook 'public_profile', please allow access to sign in to Pentor",
                                            duration: 3,
                                            presentingView: self.view)
            } else if result?.declinedPermissions.contains("email") == true {
                APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace,
                                            message: "You have denied permission to your facebook 'email', please allow access to sign in to Pentor",
                                            duration: 3,
                                            presentingView: self.view)
            } else {
                self.fetchFacebookUserProfile()
            }
        }
    }
    
    func fetchFacebookUserProfile(){
        
        if (FBSDKAccessToken.current()) != nil {
            
            FBSDKGraphRequest(graphPath: "me",
                              parameters: ["fields":"name,email,gender,first_name,last_name,locale,location,picture.type(large)"],
                              httpMethod: "GET")
                .start(completionHandler: { (connection,  result, err) in
                    let res: [String:AnyObject]? = result as? [String : AnyObject]
                    guard let email = res?["email"] as? String else{ return }
                    UserService.signUp(type: .facebook, email: email, password: "na", resultCallback: {[weak self] (result) in
                        if let aview = self?.view {
                            APESuperHUD.removeHUD(animated: true, presentingView: aview, completion: { _ in
                                // Completed
                            })
                        }
                        self?.showHomeScreen()
                    }) { (err) in
                        APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
                    }
                })
        }
    }
    
    
    @IBAction func gotoPrivacyScreen(_ sender: Any) {
        if let url = URL(string: "http://welcomematapp.org/privacy") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}

extension ViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

