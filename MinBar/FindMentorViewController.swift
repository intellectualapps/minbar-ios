//
//  FindMentorViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/22/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import Koloda
import DZNEmptyDataSet
import Alamofire
import APESuperHUD

class FindMentorViewController: BaseViewController,KolodaViewDelegate,KolodaViewDataSource,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
       return DragSpeed.default
    }

    
    lazy var kolodaView:KolodaView = {
        let view = KolodaView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    lazy var tableView:UITableView = {
        let tableView = UITableView()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    let declineButton:UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named:"close-icon2"), for: .normal)
        button.tintColor = .red
        button.layer.cornerRadius = 30
        button.layer.borderColor = UIColor(hex:"#EFEFEF").cgColor
        button.layer.borderWidth = 2
        button.backgroundColor = .white
        return button
    }()
    let acceptButton:UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named:"check-new"), for: .normal)
        button.layer.cornerRadius = 30
        button.layer.borderColor = UIColor(hex:"#EFEFEF").cgColor
        button.layer.borderWidth = 2
        button.backgroundColor = .white
        button.tintColor = UIColor(hex:"#008400")
        return button
    }()
    var user: User? = User.archivedUser()
    
    var mentors:[User]? = [] {
        didSet{
            if let count = self.mentors?.count {
                if count > 0 {
                    self.tableView.isHidden = true
                }else{
                    self.tableView.isHidden = false
                }
            } else {
                self.tableView.isHidden = false
            }
            DispatchQueue.main.async {
                self.kolodaView.reloadData()
                self.kolodaView.resetCurrentCardIndex()
            }
        }
    }
    var subInterestId:String?
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "man")
    }
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let font = UIFont(name: "Avenir-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20)
        let attribute = [NSFontAttributeName: font]
        return NSAttributedString(string: "Find Mentor", attributes: attribute)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let font = UIFont(name: "Avenir", size: 15) ?? UIFont.systemFont(ofSize: 15)
        let attribute = [NSFontAttributeName: font]
        return NSAttributedString(string: "tap.to.findmentor".localized(), attributes: attribute)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let attribute = [NSFontAttributeName:UIFont(name: "Avenir-Medium", size: 15)!,
                         NSForegroundColorAttributeName:UIColor(hex:"#168BFE")]
        return NSAttributedString(string: "button.findmentor".localized(), attributes: attribute)
    }
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -64
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        let controller = InterestViewController()
        controller.data = nil
        controller.isMentorMode = true
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        // return self.images.count
        if let count = self.mentors?.count{
            return count
        }else{
            return 0
        }
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = CardView()
        let user = mentors?[index]
        let name = user?.fullname() ?? ""
        view.profileFullName.text = name
        view.profileDescription.text = user?.longBio
        if let url = user?.profilePhotoURL {
            self.setProfileImage(str: url, imageView: view.profileImageView)
        }
        return view
    }
    
    func setProfileImage(str: String, imageView: UIImageView){
        if (str.contains("pentor-bff")) {
            Alamofire.request(str, method: .get, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (response) in
                if response.result.isSuccess {
                    guard let result = response.result.value as? Dictionary<String,AnyObject> else {
                        return
                    }
                    let base64 = result["base64ImageString"] as! String
                    let data  = Data(base64Encoded: base64, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                    DispatchQueue.main.async {
                        if let  _ = data{
                            imageView.image = UIImage(data: data!)
                            imageView.contentMode = .scaleAspectFill
                            imageView.clipsToBounds = true
                        }
                    }
                }else{
                    print(response.result.error!.localizedDescription)
                    print("error")
                }
            }
        }else{
            if let url = URL(string: str) {
                imageView.sd_setImage(with: url, placeholderImage: UIImage(named:""))
            }
        }
    }
    
    func swipeLeft(){
        kolodaView.swipe(.left)
    }
    func swipeRight(){
        kolodaView.swipe(.right)
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        switch direction {
        case .left:
            break
        case .right:
            //      let mentor = mentors?[index]
            //     self.requestConnection(mentor: mentor!, message: "hi")
            self.showTextViewEntryAlert("title.link.to.mentor".localized(), msg: "link.to.mentor.message".localized(), viewController: self, index: index)
        default:
            break
        }
        
        
    }
    func showTextViewEntryAlert(_ title:String, msg:String, viewController:UIViewController, index:Int) {
        
        let otherButtonTitle = "button.send".localized()
        let cancelButtonTitle = "button.cancel".localized()
        
        let alertController = DOAlertController(title: title, message: msg, preferredStyle: .alert)
        
        // Add the text field for text entry.
        alertController.addTextViewWithConfigurationHandler { textView in
            // If you need to customize the text field, you can do so here.
            textView?.text = "title.hi".localized()
            textView?.font = UIFont(name: "Avenir", size: 15)
        }
        
        
        
        let otherAction = DOAlertAction(title: otherButtonTitle, style: .default) { action in
            let content = (alertController.textViews?.first as? UITextView)?.text ?? ""
            if let mentor = self.mentors?[index] {
                self.requestConnection(mentor: mentor, message: content)
            }
        }
        let cancelAction = DOAlertAction(title: cancelButtonTitle, style: .cancel) { action in
            
        }
        // Add the actions.
        alertController.addAction(cancelAction)
        alertController.addAction(otherAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }

    func requestConnection(mentor: User, message: String) {
        let useremail = user?.email ?? ""
        let msg = message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "Sending...", presentingView: self.view)
        let param  = "?mentee-email=\(useremail)&mentor-email=\(mentor.email ?? "")&sub-interest-id=\(subInterestId ?? "")&initial-message=\(msg ?? "")"
        UserService.establishConnection(urlParam: param, resultCallback: {[weak self] (success) in
            if let strongself = self {
                APESuperHUD.showOrUpdateHUD(icon: IconType.checkMark, message: "Sent", duration: 3, presentingView: strongself.view)
            }
        }) { (err) in
            APESuperHUD.showOrUpdateHUD(icon: IconType.sadFace, message: err, duration: 3, presentingView: self.view)
        }
    }
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        //koloda.resetCurrentCardIndex()
        tableView.isHidden = false
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        print("tapped")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
    }
    
    func configureView(){
        view.addSubview(kolodaView)
        kolodaView.translatesAutoresizingMaskIntoConstraints = false
        kolodaView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 32).isActive = true
        kolodaView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -32).isActive = true
        kolodaView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
        kolodaView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -100).isActive = true
        addButtons()
        declineButton.addTarget(self, action: #selector(swipeLeft), for: .touchUpInside)
        acceptButton.addTarget(self, action: #selector(swipeRight), for: .touchUpInside)
        self.view.backgroundColor = UIColor(hex: "#F6F6F6")
        self.view.addSubview(tableView)
        tableView.leftAnchor.constraint(equalTo:self.view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo:self.view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo:self.view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:self.view.bottomAnchor).isActive = true
        tableView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(mentors(_:)), name: NSNotification.Name("FoundMentors"), object: nil)
        
        if let count = self.mentors?.count{
            if count > 0{
                self.tableView.isHidden = true
            }else{
                self.tableView.isHidden = false
            }
        }
        
    }
    
    
    func mentors(_ notification:Notification){
        let userInfo = notification.userInfo
        self.mentors = userInfo?["results"] as? [User]
        self.subInterestId = userInfo?["sub-interest-id"] as? String
    }
    
    func addButtons(){
        self.view.addSubview(declineButton)
        self.view.addSubview(acceptButton)
        
        declineButton.rightAnchor.constraint(equalTo: self.view.centerXAnchor, constant: -10).isActive = true
        declineButton.topAnchor.constraint(equalTo: kolodaView.bottomAnchor, constant: 10).isActive = true
        declineButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        declineButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        acceptButton.leftAnchor.constraint(equalTo:self.view.centerXAnchor, constant: 10).isActive = true
        acceptButton.topAnchor.constraint(equalTo: kolodaView.bottomAnchor, constant: 10).isActive = true
        acceptButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        acceptButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "button.findmentor".localized()
        if let count = self.mentors?.count{
            if count > 0 {
                self.tableView.isHidden = true
                DispatchQueue.main.async {
                    self.kolodaView.reloadData()
                    self.kolodaView.resetCurrentCardIndex()
                }
            }else{
                self.tableView.isHidden = false
            }
        }else{
            self.tableView.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
