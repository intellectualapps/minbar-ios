//
//  ManageMentorController.swift
//  PentorApp
//
//  Created by Segun Solaja on 3/12/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import APESuperHUD

class ManageMentorController: BaseViewController {
    var user: User? = User.archivedUser()
    
    @IBOutlet weak var connectionTable: UITableView!
    var connections:[UserConnection]? = [] {
        didSet{
            connectionTable.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        
    }
    
    func configureView(){
        
        connectionTable.dataSource = self
        connectionTable.delegate = self
        connectionTable.tableFooterView = UIView(frame: CGRect.zero)
        connectionTable.estimatedRowHeight = 79
        connectionTable.rowHeight = UITableViewAutomaticDimension
        let rightButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action: #selector(reload))
        self.navigationItem.rightBarButtonItem = rightButton
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("reload"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("connectionRequest"), object: nil)
    }

    func connectionRequest() {
        DispatchQueue.main.async {
            self.reload()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "title.manageship".localized()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getConnections(isNeedProgress: !viewDidAppeared)
        self.viewDidAppeared = true
    }

    func reload() {
        DispatchQueue.main.async {
            self.getConnections(isNeedProgress: true)
        }
    }
    
    func getConnections(isNeedProgress: Bool = true){
        if isNeedProgress {
            APESuperHUD.showOrUpdateHUD(loadingIndicator: .standard, message: "title.loading".localized(), presentingView: self.view)
        }
        if let email = user?.email {
            UserService.getConnections(urlParam: ["email": email], resultCallback: { (connections) in
                self.connections = connections
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                    
                })
            }, errorCallback: { (err) in
                APESuperHUD.removeHUD(animated: true, presentingView: self.view, completion: { _ in
                    
                })
                
            })
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ManageMentorController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.connections?.count{
            return count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.connectionTable.dequeueReusableCell(withIdentifier: "connectionCell") as? ConnectionCell else {
            return UITableViewCell()
        }
        guard let connection = self.connections?[indexPath.row] else {
            return UITableViewCell()
        }
        guard let mentee = connection.mentee else {
            return UITableViewCell()
        }
        guard let mentor = connection.mentor else {
            return UITableViewCell()
        }
        let menteeEmail = mentee.email ?? ""
        let useremail = user?.email ?? ""
        cell.userFullName.text = menteeEmail == useremail ? mentor.fullname() : mentee.fullname()
        cell.userBio.text = menteeEmail == useremail ? mentor.shortBio : mentee.shortBio
        cell.userType.text = menteeEmail == useremail ? "title.mentor".localized() : "title.mentee".localized()
        let url =  menteeEmail == useremail ? mentor.profilePhotoURL : mentee.profilePhotoURL
        if let _url = url{
            cell.userImage.sd_setImage(with: URL(string:_url), placeholderImage: UIImage(named:"man"))
        }
        cell.userImage.clipsToBounds = true
        cell.userImage.contentMode = .scaleAspectFill
        cell.userImage.layer.cornerRadius = cell.userImage.frame.width / 2
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let connection = self.connections?[indexPath.row] else {
            return
        }
        guard let mentee = connection.mentee else {
            return
        }
        guard let mentor = connection.mentor else {
            return
        }
        let menteeEmail = mentee.email ?? ""
        let mentorEmail = mentor.email ?? ""
        let email = user?.email ?? ""
        let userFullName =  menteeEmail == email ? mentor.fullname() : mentee.fullname()
        let userEmail =  menteeEmail == email ? mentorEmail : menteeEmail

        if let status = connection.currentStatus {
            if status == "ACTIVE" {
                let viewController = ChatViewController(collectionViewLayout: UICollectionViewFlowLayout())
                viewController.title = "Chat"
                viewController.userFullName = userFullName 
                viewController.connectionId = connection.connectionId ?? ""
                viewController.email = userEmail
                viewController.view.backgroundColor = .white
                self.navigationItem.title = ""
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                let _user = menteeEmail == email ? mentor : mentee
                let isMentee = menteeEmail == email ? true : false
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "userConnection") as? UserConProfileController {
                    controller.isMentee = isMentee
                    controller.user = _user
                    controller.parentController = self
                    controller.connectionId = connection.connectionId ?? ""
                    self.navigationItem.title = ""
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
}
