//
//  ChatInputView.swift
//  PentorApp
//
//  Created by Segun Solaja on 5/7/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import UITextView_Placeholder

class ChatInputView: UIView,UITextViewDelegate {
    
    weak var chatView: ChatViewController? {
        didSet {
            sendButton.addTarget(chatView, action: #selector(ChatViewController.handleSend), for: .touchUpInside)
        }
    }
    
    let defaulfTopAndBottomMargin:CGFloat = 10
    lazy var inputTextField: UITextView = {
        let textField = UITextView()
        textField.placeholder = "enter.message".localized()
        textField.font = UIFont(name: "Avenir", size: 18)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.isScrollEnabled = false
        textField.delegate = self
        return textField
    }()
    
    let sendButton = UIButton(type: .system)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        self.autoresizingMask = [.flexibleHeight , .flexibleBottomMargin]
        
        sendButton.setTitle("button.send".localized(), for: .normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(sendButton)
        
        //x,y,w,h
        sendButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        sendButton.topAnchor.constraint(equalTo: topAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        sendButton.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        addSubview(self.inputTextField)
        
        //x,y,w,h
        self.inputTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        self.inputTextField.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        self.inputTextField.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        
        let separatorLineView = UIView()
        separatorLineView.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(separatorLineView)
        //x,y,w,h
        separatorLineView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        separatorLineView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.inputTextField.invalidateIntrinsicContentSize()
        self.reloadInputViews()
        let text = (textView.text ?? "").trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.sendButton.isEnabled = text.characters.count > 0
    }
    
    
    
    override var intrinsicContentSize: CGSize{
        let textSize = self.inputTextField.sizeThatFits(CGSize(width: self.inputTextField.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        return CGSize(width: self.bounds.width, height: textSize.height + defaulfTopAndBottomMargin)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
