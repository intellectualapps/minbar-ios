//
//  MButton.swift
//  PentorApp
//
//  Created by Nguyen Van Dung on 8/15/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import Foundation
import UIKit

class MButton: UIButton {
    var extendedEdges: UIEdgeInsets?

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let relativeFrame = self.bounds
        let top = -(extendedEdges?.top ?? 0)
        let left = -(extendedEdges?.left ?? 0)
        let right = -(extendedEdges?.right ?? 0)
        let bottom = -(extendedEdges?.bottom ?? 0)
        let hitTestEdgeInsets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitTestEdgeInsets)
        return hitFrame.contains(point)
    }
}
