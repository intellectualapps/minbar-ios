//
//  UserConnection.swift
//  PentorApp
//
//  Created by Segun Solaja on 3/23/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class UserConnection: NSObject,NSCoding  {
    var connectionId:String?
    var createdDate:Date?
    var currentStatus:String?
    var mentee:User?
    var mentor:User?
    var subInterestId:String?
    
    override init () {
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(connectionId, forKey: "connectionId")
        aCoder.encode(createdDate, forKey: "createdDate")
        aCoder.encode(currentStatus, forKey: "currentStatus")
        aCoder.encode(mentee, forKey: "mentee")
        aCoder.encode(mentor, forKey: "mentor")
        aCoder.encode(subInterestId, forKey: "subInterestId")
    }
    
    required init(coder aDecoder: NSCoder) {
        self.connectionId = aDecoder.decodeObject(forKey: "connectionId") as? String
        self.createdDate = aDecoder.decodeObject(forKey: "createdDate") as? Date
        self.currentStatus = aDecoder.decodeObject(forKey: "currentStatus") as? String
        self.mentee = aDecoder.decodeObject(forKey: "mentee") as? User
        self.mentor = aDecoder.decodeObject(forKey: "mentor") as? User
        self.subInterestId = aDecoder.decodeObject(forKey: "subInterestId") as? String
    }
}
