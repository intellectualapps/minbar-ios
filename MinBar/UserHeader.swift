//
//  UserHeader.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/5/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class UserHeader: UIView {
    
    let userImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "man")
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 30
        image.layer.borderColor = UIColor.white.cgColor
        image.layer.borderWidth = 2
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let userFullNameLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "title.user".localized()
        label.textColor = UIColor.white
        label.font = UIFont(name: "Avenir-Heavy", size: 16)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(userImage)
        addSubview(userFullNameLabel)
        setupContraints()
    }
    
    func setupContraints(){
        userImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        userImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        userImage.widthAnchor.constraint(equalToConstant: 60).isActive = true
        userImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        userFullNameLabel.leftAnchor.constraint(equalTo: userImage.rightAnchor, constant: 8).isActive = true
        userFullNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        userFullNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
