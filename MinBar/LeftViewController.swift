//
//  LeftViewController.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/5/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit
import SASlideMenu
import Alamofire

class LeftViewController: SASlideMenuViewController,SASlideMenuDataSource,SASlideMenuDelegate {
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        displayUserProfile()
    }

    var header:UserHeader!
    var user: User? = User.archivedUser()
    
    func configureView(){
        UIApplication.shared.isStatusBarHidden = false
        self.tableView?.tableFooterView  = UIView(frame: CGRect.zero)
        self.tableView?.tableHeaderView = getUserHeader()
        self.tableView?.separatorStyle = .none
        self.tableView?.tableHeaderView?.backgroundColor = UIColor.clear
        self.tableView?.backgroundColor = UIColor(hex: "#2B5CAC")
        NotificationCenter.default.addObserver(self, selector: #selector(selectFindMentor), name: NSNotification.Name("RefreshProfile"), object: nil)
        self.slideMenuDelegate = self
        self.updatePushToken()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    func refresh(){
        if let usr = user {
            header.userFullNameLabel.text = usr.fullname()
            if let profileURL = usr.profilePhotoURL {
                self.setProfileImage(str: profileURL, imageView: header.userImage)
            }
        }
    }
    
    func slideMenuDidSlide(toLeft selectedContent: UINavigationController!) {
        print("here")
    }
    func selectFindMentor(){
        self.selectContent(at: IndexPath(item: 1, section: 0), scrollPosition: .none)
    }
    
    
    
    
    func getUserHeader() -> UserHeader{
        header = UserHeader(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 180))
        if let usr = user {
            header.userFullNameLabel.text = usr.fullname()
            if let profileURL = usr.profilePhotoURL {
                self.setProfileImage(str: profileURL, imageView: header.userImage)
            }
        }
        header.backgroundColor = UIColor.clear
        return header
    }

    func displayUserProfile() {
        if let usr = user {
            header.userFullNameLabel.text = usr.fullname()
            if let profileURL = usr.profilePhotoURL {
                self.setProfileImage(str: profileURL, imageView: header.userImage)
            }
        }
    }
    
    func setProfileImage(str:String,imageView:UIImageView){
        if (str.contains("pentor-bff")) {
            Alamofire.request(str, method: .get, parameters: nil, encoding: URLEncoding.default, headers: Constants.headerConstants([:])).responseJSON { (response) in
                if response.result.isSuccess {
                    if let result = response.result.value as? Dictionary<String,AnyObject> {
                        if let base64 = result["base64ImageString"] as? String {
                            let data  = Data(base64Encoded: base64, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)
                            DispatchQueue.main.async {
                                imageView.image = UIImage(data: data!)
                            }
                        }
                    }
                } else {
                    print(response.result.error!.localizedDescription)
                    print("error")
                }
            }
        }else{
            if let url = URL(string: str) {
                imageView.sd_setImage(with: url, placeholderImage: UIImage(named:""))
            }
        }
    }
    
    func selectedIndexPath() -> IndexPath! {
        if let usr = user {
            let fname = (usr.firstName ?? "").lowercased()
            let lname = (usr.lastName ?? "").lowercased()
            if fname == "" && lname == "" {
                return IndexPath(item: 2, section: 0)
            } else if fname != "na" &&  lname != "na" {
                return IndexPath(item: 1, section: 0)
            } else {
                return IndexPath(item: 2, section: 0)
            }
        } else {
            return IndexPath(item: 2, section: 0)
        }
        
    }
    
    func  segueId(for indexPath: IndexPath!) -> String {
        var stringIndex = ""
        print(indexPath.row)
        switch(indexPath.row){
        case 0 : stringIndex = "manageMentor"
        case 1 :stringIndex = "findMentor"
        case 2 :stringIndex = "profile"
        default : stringIndex = "logout"
            
        }
        return stringIndex
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if(identifier == "BRa-4K-1p0.text".localized() ){
            return false
        }else{
            return true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "profile"{
            let profileNav = segue.destination as! UINavigationController
            if let _ = user{
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "basicInfo") as! BasicInfoViewController
                controller.isCreateProfileMode = false
                profileNav.viewControllers[0] =  controller
            }else{
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "profileController") as! ProfileViewController
                profileNav.viewControllers[0] =  controller
            }
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 3){
            UserDefaults.standard.removeObject(forKey: "user")
            let controller = self.storyboard?.instantiateInitialViewController()
            self.view.window?.rootViewController = controller
        }else{
            self.selectContent(at: indexPath, scrollPosition: .none)
        }
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func configureMenuButton(_ menuButton: UIButton!) {
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 29);
        menuButton.setImage(UIImage(named:"menu-icon"), for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updatePushToken(){
        if let token = appDelegate.pushToken {
            let email = user?.email ?? ""
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            let urlParam = "\(email)?token=\(token)&uuid=\(deviceID)&platform=2"
            UserService.updatePushToken(urlParam: urlParam, resultCallback: { (result) in
                print(result)
            }, errorCallback: { (err) in
                print(err)
            })
        }
    }
}
