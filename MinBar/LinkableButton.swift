//
//  LinkableButton.swift
//  Mosaic
//
//  Created by Nguyen Van Dung on 7/31/17.
//  Copyright © 2017 Dht. All rights reserved.
//

import Foundation
import UIKit

class LinkableButton: MButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        let currentTitle = self.currentTitle
        self.setTitle(currentTitle, for: .normal)
    }

    override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle(title, for: state)
        self.setAttributedTitle(self.attributedString(), for: .normal)
    }

    private func attributedString() -> NSAttributedString? {
        let font = self.titleLabel?.font ?? UIFont.systemFont(ofSize: 16)
        let textColor = self.titleLabel?.textColor ?? UIColor.white
        let attributes = [
            NSFontAttributeName : font,
            NSForegroundColorAttributeName : textColor,
            NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue
        ] as [String : Any]
        let title = self.currentTitle ?? ""
        let attributedString = NSAttributedString(string: title, attributes: attributes)
        return attributedString
    }
}
