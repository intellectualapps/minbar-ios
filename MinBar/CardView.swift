//
//  CardView.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/22/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class CardView: UIView {

    let profileImageView:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "testImage")
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        image.layer.shouldRasterize = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    let profileFullName:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir-Heavy", size: 15)
        label.text = "profile.name".localized()
        label.textColor = .darkGray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let profileDescription:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Avenir", size: 13)
        label.textColor = .gray
        label.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        self.layer.cornerRadius = 4
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.shouldRasterize = true
        self.layer.borderColor = UIColor(hex:"#CCCCCC").cgColor
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true
        addSubview(profileImageView)
        addSubview(profileFullName)
        addSubview(profileDescription)
        setupConstraints()
    }

    func setupConstraints(){
        profileImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        profileImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        profileImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        let lower = DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPAD
        profileImageView.heightAnchor.constraint(equalToConstant: lower ? 190 : 200).isActive = true

        profileFullName.leftAnchor.constraint(equalTo: leftAnchor,constant: 16).isActive = true
        profileFullName.rightAnchor.constraint(equalTo: rightAnchor,constant: -16).isActive = true
        profileFullName.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant:16).isActive = true
        profileFullName.heightAnchor.constraint(equalToConstant: 20).isActive = true

        profileDescription.leftAnchor.constraint(equalTo: profileFullName.leftAnchor).isActive = true
        profileDescription.rightAnchor.constraint(equalTo: profileFullName.rightAnchor).isActive = true
        profileDescription.topAnchor.constraint(equalTo: profileFullName.bottomAnchor,constant:4).isActive = true
        profileDescription.bottomAnchor.constraint(equalTo: bottomAnchor,constant:-16).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
